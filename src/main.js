import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false



/**
 * Automatically register components placed in the components directory
 * No need to inject them in each component. 
 * Solution was obtained from the 
 * VueMastery Video titled  "7 Tricks Vue developers don't want you to know"
 */
import {upperFirst, camelCase} from 'lodash'
const requireComponent = require.context(
  './components', false, /.+\.vue$/
)
requireComponent.keys().forEach(filename => {
  const componentConfig = requireComponent(filename)
  const componentName = upperFirst(
    camelCase(filename.replace(/^\.\//, '').replace(/\.\w+$/, ''))
  )

  Vue.component(componentName, componentConfig.default || componentConfig)
})


new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
