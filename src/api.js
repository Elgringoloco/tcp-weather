/**
 * 
 * In this application I wanted to play around with the Await/Async pattern instead
 * of the Promise pattern. The resources I reviewed are below
 * 
 * https://dev.to/shoupn/javascript-fetch-api-and-using-asyncawait-47mp
 * https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous/Async_await
 */
const API_KEY = process.env.VUE_APP_API_KEY;
const API_SERVER = "https://api.openweathermap.org/data/2.5"

async function fetchCurrentWeatherByZipCode(zipcode) {
    const fullURI = `${API_SERVER}/weather?zip=${zipcode},us&APPID=${API_KEY}`
    const response = await fetch(fullURI);
    const data = await response.json();
    return data;
}

async function fetchForecastByZipCode(zipcode) {
    const fullURI = `${API_SERVER}/forecast?zip=${zipcode},us&APPID=${API_KEY}`
    const response = await fetch(fullURI);
    const data = await response.json();
    return data;
}

export default {
    fetchForecastByZipCode,
    fetchCurrentWeatherByZipCode
}