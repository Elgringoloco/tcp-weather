import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import API from './api.js'

import { groupBy, upperFirst } from 'lodash'
import { kelvinToFahrenheit, greaterThanFiveMinutes } from './common/utils'

// Action definitions
const FETCH_FORECAST = 'FETCH_FORECAST';
const FETCH_CURRENT_WEATHER = 'FETCH_CURRENT_WEATHER';
const SET_CURRENT_WEATHER = 'SET_CURRENT_WEATHER';
const SET_FORECAST = 'SET_FORECAST'
const SET_UPDATED = 'SET_UPDATED'


export default new Vuex.Store({
  state: {
    currentWeather: {

    },
    forecasts: {

    },
    lastUpdated: ''
  },
  mutations: {
    [SET_CURRENT_WEATHER](state, { zipcode, payload }) {
      // Since the state item currentWeather (and forecasts)
      // are maps, they do not automatically gain reactivity by 
      // Vue. Using Vue#set() indicates that they need to be reactive
      // https://stackoverflow.com/questions/55180530/vuex-getter-not-updating-on-state-changes
      Vue.set(state.currentWeather, zipcode, payload)
    },
    [SET_FORECAST](state, { zipcode, payload }) {
      Vue.set(state.forecasts, zipcode, payload)
    },
    [SET_UPDATED](state, timestamp) {
      state.lastUpdated = timestamp
    }
  },
  actions: {
    [FETCH_CURRENT_WEATHER]({ commit, state }, { zipcode, force }) {
      // Guard against making API calls if the data has been fetched within th last
      // 5 minutes (unless the user opts to force the API call)
      if (!state.currentWeather[zipcode] || greaterThanFiveMinutes(state.lastUpdated) || force) {
        API.fetchCurrentWeatherByZipCode(zipcode)
          .then((payload) => {
            const updated = Date.now()
            commit(SET_UPDATED, updated)
            commit(SET_CURRENT_WEATHER, { zipcode, payload })
          })
      }
    },
    [FETCH_FORECAST]({ commit, state }, { zipcode, force }) {
      if (!state.forecasts[zipcode] || greaterThanFiveMinutes(state.lastUpdated) || force) {
        API.fetchForecastByZipCode(zipcode)
          .then(payload => {
            const updated = Date.now()
            commit(SET_UPDATED, updated)
            commit(SET_FORECAST, { zipcode, payload })
          })
      }
    }
  },
  getters: {
    getCurrentWeatherSummary: (state) => (zipcode) => {
      const currentWeather = state.currentWeather[zipcode]
      if (currentWeather) {
        return {
          description: upperFirst(currentWeather.weather.map(w => w.description).join(', ')),
          temperature: kelvinToFahrenheit(currentWeather.main.temp),
          icon: `https://openweathermap.org/img/wn/${currentWeather.weather[0].icon}@2x.png`
        }
      } else {
        return {
          description: 'Loading description',
          temperature: 'Loading temperature'
        }
      }
    },
    getDetailedSummary: (state) => (zipcode) => {
      const currentWeather = state.currentWeather[zipcode]
      if (currentWeather) {
        return {
          hasData: true,
          city: currentWeather.name,
          description: upperFirst(currentWeather.weather.map(w => w.description).join(', ')),
          humidity: currentWeather.main.humidity + ' %',
          pressure: currentWeather.main.pressure + ' hPa',
          windspeed: currentWeather.wind.speed + ' m/s',
          winddegree: currentWeather.wind.deg,
          temperature: kelvinToFahrenheit(currentWeather.main.temp) + ' °F',
          icon: `https://openweathermap.org/img/wn/${currentWeather.weather[0].icon}@2x.png`
        }
      } else {
        return {
          hasData: false
        }
      }
    },
    getForecastSummaries: (state) => (zipcode) => {
      const forecast = state.forecasts[zipcode]
      if (forecast) {
        // item.dt_text is a datetime string. 
        // The forcast.list is a collection of variable amount
        // of weather objects for each day. 
        // Extract the info to be displayed and then group them by date
        return groupBy(forecast.list.map(item => {
          return {
            date: item.dt_txt.slice(0, 10),
            time: item.dt_txt.slice(11, 16),
            temperature: kelvinToFahrenheit(item.main.temp) + ' °F',
          }
        }), 'date')
      }
      else {
        return {}
      }
    }
  }
})
