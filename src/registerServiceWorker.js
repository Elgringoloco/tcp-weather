/* eslint-disable no-console */

/**
 * This is the basic configuration generated by the Vue/CLI 
 *  It will cache the resources used by the application when it that 
 *  are served on the main domain.
 * 
 *  It could probably be extended to handle content / information from
 *  other domains if is conigured properly. Need to read more on it 
 *  at https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API
 *  and https://www.npmjs.com/package/@vue/cli-plugin-pwa 
 */
import { register } from 'register-service-worker'

if (process.env.NODE_ENV === 'production') {
  register(`${process.env.BASE_URL}service-worker.js`, {
    ready () {
      console.log(
        'App is being served from cache by a service worker.\n' +
        'For more details, visit https://goo.gl/AFskqB'
      )
    },
    registered () {
      console.log('Service worker has been registered.')
    },
    cached () {
      console.log('Content has been cached for offline use.')
    },
    updatefound () {
      console.log('New content is downloading.')
    },
    updated () {
      console.log('New content is available; please refresh.')
    },
    offline () {
      console.log('No internet connection found. App is running in offline mode.')
    },
    error (error) {
      console.error('Error during service worker registration:', error)
    }
  })
}
