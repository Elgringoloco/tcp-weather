
/**
 * Conversion algorithms adapted from this SO 
 * https://stackoverflow.com/questions/28869138/temp-converter-using-javascript
 * 
 */
export function kelvinToCelsius(value) {
    return value - 273.15;
}
export function kelvinToFahrenheit(value) {
    return (1.8 * kelvinToCelsius(value) + 32).toPrecision(2);
}
/**
 * Adapted from https://stackoverflow.com/questions/9224773/js-check-if-date-is-less-than-1-hour-ago
 */
export function greaterThanFiveMinutes(lastUpdated) {
    if (!lastUpdated) {
        return true
    }
    const FIVE_MINUTES = 1000 * 60 * 5;
    const fiveMinutesAgo = Date.now() - FIVE_MINUTES;
    return lastUpdated < fiveMinutesAgo
}