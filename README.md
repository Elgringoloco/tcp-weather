# TCP Weather Example

A working demo of the site can be found at [https://weather.travispence.com](https://weather.travispence.com)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Environment

An API key for OpenWeatherMap is needed and can be defined in the `.env` file.

https://weather.travispence.comre
```
.env    # Environment variables defined here
...     # Other config files
public/
├── index.html  # Vue Application gets injected here
├── mainfest.json   # Registration for PWA
├── favicon.ico
└── ... # other public assets
src/ 
├── main.js # Bootstrap the Vue Application
├── api.js  # abstractions for making API requests
├── App.vue  # Entry point for the Vue Application
├── registerServiceWorker.js    # Register Service Worker for PWA
├── store.js    # Vuex Store to handle state and asynchronous actions
├── router.js    # Route definitions
├── assets  # project assets
│   ├── ... 
├── components   # Reusable components 
│   ├── TcpWeatherList.vue
│   └── ...    
├── common  # shared functionality
│   ├── utils.js 
├── views   # Top Level Views that contain components              
│   ├── Home.vue    
│   └── WeatherDetails.vue  
├── plugins # register and configure the various plugins used by vue on a global level
    └──  vuetify.vue 

```
### Future Improvements

*  Utilize `localStorage` to persist OpenWeatherAPI requests
*  Configure the `ServiceWorker` to cache the images from OpenWeatherMap or replace with images controlled by this project. 
*  add ability to toggle between Light/Dark Theme mode
*  Update the Forecasted temperature data
*  Include more data from the API response in the detailed view.
*  Allow for the User to add a city/zipcode pair to the list
*  Display the DateTimesString for when the data was last fetched from the API
*  Allow the user to forcefully refectch the data from the API
*  Make sure the application runs the target browsers and devices

### How to improve effectiveness of User Experience and how to solicit feedback from User

* Providing the user with training, video walkthroughs, or an FAQ can help improve the user experience.
* The best way to solicit feedback would be to sit down with a real user of the application and observe how they use the app. Often they use the application in an entirely different way than the programmer anticipates.
* If unable to sit down with the user the next best option would be to communicate over the phone, allowing the user to walk you through their workflow step-by-step. 
* If the user is a bit technical they may be able to send a screen recording or screen shot of what they are experiencing. 
* Email / Web Form  solicitations in text can be harder to ascertain all of the necessary details to recreate the issue the user is experiencing but would still be of value.

**Important items to keep in mind**
*  The various browsers and devices use different JavaScript engines and may not support some of the methods used in the application. (same goes for CSS)
 * Not only is it important to know which browser/device is being used, but which version as well.
*  Different screen sizes can affect the presentation of the application
*  Where the application is being used  (e.g. outside in the brightsun or in low light conditions) can affect the ability to view the application's presentation.

